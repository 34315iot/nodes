/*
 *  HomeSensorNetwork:nodes src code
 * 
 * 
 */


#include <ESP8266WiFi.h>
#include <PubSubClient.h>


unsigned long previousMillis = 0; 
const long interval = 2000; 

/*
const char* ssid = "KANET";
const char* password = "kopluslort21";
const char* mqtt_server = "192.168.1.192";*/

const char* ssid = "hitech360";
const char* password = "ABixeNH4HleMv72v";
const char* mqtt_server = "192.168.42.1";

float humidity, temp_f;
//const char* mqtt_username = "put your X-ACCESS-TOKEN here";
//const char* mqtt_password = "put your Device Secret here";

const char* mqtt_topic = "test";

long randNumber;


WiFiClient espClient;
PubSubClient client(espClient);


String dataID;
String dataTemp;
char charBufID[100];
char charBufTemp[100];
char charBufTest[100];
void setup() {
  // setup serial port
  Serial.begin(115200);

  // setup WiFi
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
  

}

void setup_wifi() {
  delay(10);
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();
}

void reconnect() {
  //WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
    setup_wifi();
  }
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    if (client.connect("ESP8266_Client")) {
      Serial.println("connected");
    } else {
      Serial.print("MQTT connection failed, retry count: ");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      delay(5000);
    }
  }
}


void loop() {
  if (!client.connected()) {
    reconnect();
  }
  client.loop();
  
  dataID = ESP.getChipId();
  dataID.toCharArray(charBufID, 150);
  Serial.println(charBufID);
  randNumber = random(3, 40);
  String thisString = String(randNumber);
  dataTemp = thisString;
  dataTemp.toCharArray(charBufTemp, 150);
  Serial.println(charBufTemp);
  String test = "iot/temp/" + dataID;
  Serial.println(test);
  test.toCharArray(charBufTest, 150);
  client.publish(charBufTest, charBufTemp);
  Serial.println( "Closing MQTT connection...");
  client.disconnect();
  Serial.println( "Closing WiFi connection...");
  WiFi.disconnect();
  Serial.println( "Sleeping for a minute");
  delay(3000);
}
